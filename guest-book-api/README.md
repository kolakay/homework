# Visitors Guest Book API

> ### Lumen codebase that performs CRUD, on guest book requests.

This repo is functionality complete — PRs and issues welcome!

----------

# Getting started

## Installation

Please check the official Lumen installation guide for server requirements before you start. [Official Documentation](https://lumen.laravel.com/docs/6.x#installation)


Clone the repository

    git clone git@bitbucket.org:kolakay/homework.git

Switch to the repo folder

    cd homework/guest-book-api

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone git@bitbucket.org:kolakay/homework.git
    cd homework/guest-book-api
    composer install
    cp .env.example .env
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate
    php artisan serve

## Database seeding

**Populate the database with seed data with relationships which has guests. This can help you to quickly start testing the api or couple a frontend and start using it with ready content.**

Open the GuestSeeder and set the property values as per your requirement

    database/seeds/GuestSeeder.php

Run the database seeder and you're done

    php artisan db:seed

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh

## Environment variables

- `.env` - Environment variables can be set in this file

***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.

----------

# Testing API

Run the laravel development server

    php artisan serve

The api can now be accessed at

    http://localhost:8000/

## API Specification

This application adheres to the api specifications set by the author

> [Full API Spec](https://guestbookapi.testmynewwork.website/docs)
----------