<?php

namespace App\Http\Controllers;

use Log;
use Illuminate\Http\Request;
use App\Models\GuestModel;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @response {
     *  "status" : "success",
     *  "guests": [{
     *      "id": 4,
     *      "name": "Jessica Jones",
     *      "email": "janey@doe.com",
     *      "comment": "Hey I love your work and I want to see you"
     *  }] 
     * }
     * @response 500 {
     *  "status": "error",
     *  "message": "Unable to get guests! Encountered an error"
     * }
     */
    public function index()
    {
        try{
            
            $guests = GuestModel::all();
            return response()->json([
                "status" => "success",
                "guests" => $guests
            ], 200);

        }catch(\Exception $error){
            Log::info($error->getMessage());
            $message = "Unable to get guests! Encountered an error";
            return $this->handleError($message, [], 500);
        }
        
    }

    /**
     * @bodyParam name string required The name of the guest. Example: Olamide Badoo
     * @bodyParam email string required email of the guest. Example: jamesbond@kola.com
     * @bodyParam comment string required comment of the guest. Example: Hii I am 10x Engineer
     * @response {
     *  "status" : "success",
     *  "message": "guest created!" 
     * }
     * @response 400 {
     *  "status" : "error",
     *  "message": "Invalid  request!",
     *  "errors" : [] 
     * }
     * @response 500 {
     *  "status": "error",
     *  "message": "Unable to get guests! Encountered an error"
     * }
     */
    public function create(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:guests',
                'comment' => 'required',
            ]);
            if ($validator->fails()){
                $message = "Invalid  request! All fields are required, email must be unique";
                return $this->handleError($message, $validator->messages(), 400);
            }
            
            $guest = new GuestModel;
            $guest->name = $request->name;
            $guest->email = $request->email;
            $guest->comment = $request->comment;
            $guest->save();


            return response()->json([
                "status" => "success",
                "guest" => $guest,
                "message" => "guest created!" 
            ], 200);

        }catch(\Exception $error){
            Log::info($error->getMessage());
            $message = "Unable to create guest! Encountered an error";
            return $this->handleError($message, [], 500);
        }
        
    }

    /**
     * @bodyParam name string required The name of the guest. Example: Olamide Badoo
     * @bodyParam email string required email of the guest. Example: jamesbond@kola.com
     * @bodyParam comment string required comment of the guest. Example: Hii, I'm a 10x Engineer
     * @response {
     *  "status" : "success",
     *  "message": "guest created!" 
     * }
     * @response 400 {
     *  "status" : "error",
     *  "message": "Invalid  request!",
     *  "errors" : [] 
     * }
     * @response 500 {
     *  "status": "error",
     *  "message": "Unable to get guests! Encountered an error"
     * }
     */
    public function update(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'name' => 'required',
                'email' => 'required|email|unique:guests,email,'.$request->id,
                'comment' => 'required',
            ]);

            if ($validator->fails()){
                $message = "Invalid  request! All fields are required, email must be unique";
                return $this->handleError($message, $validator->messages(), 400);
            }
            
            $guest = GuestModel::where('id', $request->id)->first();
            
            if(!$guest){
                $message = "guest not found!";
                return $this->handleError($message, [], 404);
            }

            $guest->name = $request->name;
            $guest->email = $request->email;
            $guest->comment = $request->comment;
            $guest->save();


            return response()->json([
                "status" => "success",
                "message" => "guest updated!", 
                "guests" => GuestModel::all()
            ], 200);

        }catch(\Exception $error){
            Log::info($error->getMessage());
            $message = "Unable to create guest! Encountered an error";
            return $this->handleError($message, [], 500);
        }
        
    }

    /**
     * @queryParam guestId required The id of the guest.
     *
     * @response {
     *  "status" : "success",
     *  "guest": {
     *      "id": 4,
     *      "name": "Jessica Jones",
     *      "email": "janey@doe.com",
     *      "comment": "Hey I love your work and I want to see you"
     *  } 
     * }
     * @response 404 {
     *  "status": "error",
     *  "message": "guest not found!"
     * }
     * @response 500 {
     *  "status": "error",
     *  "message": "Unable to get guest! Encountered an error"
     * }
     */
    public function guest($guestId)
    {
        try{
            
            $guest = GuestModel::where('id', $guestId)->first();
            
            if(!$guest){
                $message = "guest not found!";
                return $this->handleError($message, [], 404);
            }

            return response()->json([
                "status" => "success",
                "guest" => $guest
            ], 200);

        }catch(\Exception $error){
            Log::info($error->getMessage());
            $message = "Unable to get guests! Encountered an error";
            return $this->handleError($message, [], 500);
        }
        
    }

     /**
     * @queryParam guestId required The id of the guest.
     *
     * @response {
     *  "status" : "success",
     *  "message": "User deleted"
     * }
     * @response 404 {
     *  "status": "error",
     *  "message": "guest not found!"
     * }
     * @response 500 {
     *  "status": "error",
     *  "message": "Unable to delete guest! Encountered an error"
     * }
     */
    public function deleteGuest(Request $request)
    {
        try{

            $guest = GuestModel::where('id', $request->id)->first();
            
            if(!$guest){
                $message = "guest not found!";
                return $this->handleError($message, [], 404);
            }

            $guest->delete();

            return response()->json([
                "status" => "success",
                "message"=> "User deleted" 
            ], 200);

        }catch(\Exception $error){
            Log::info($error->getMessage());
            $message = "Unable to delete guest! Encountered an error";
            return $this->handleError($message, [], 500);
        }
        
    }

    private function handleError($message, $errors = [], $code){
        return response()->json([
                "status" => "error",
                "message" => $message,
                "errors" => $errors
            ], $code);
    }
}
