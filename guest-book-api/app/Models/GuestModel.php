<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuestModel extends Model
{
    protected $table = "guests";

    protected $hidden = [
    	'created_at',
    	'updated_at'
    ];
}
