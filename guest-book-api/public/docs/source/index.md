---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_2d6eff3266c1ff3364098bb430dce30e -->
## /guests
> Example request:

```bash
curl -X GET -G "/guests" 
```

```javascript
const url = new URL("/guests");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": "success",
    "guests": [
        {
            "id": 4,
            "name": "Jessica Jones",
            "email": "janey@doe.com",
            "comment": "Hey I love your work and I want to see you"
        }
    ]
}
```
> Example response (500):

```json
{
    "status": "error",
    "message": "Unable to get guests! Encountered an error"    
}
```

### HTTP Request
`GET /guests`


<!-- END_2d6eff3266c1ff3364098bb430dce30e -->
<!-- START_52a8a371e7a1dc53ea827dae3ae65cca -->
## /guests/get/{guestId}
> Example request:

```bash
curl -X GET -G "/guests/get/1?guestId=aut" 
```

```javascript
const url = new URL("/guests/get/1");

    let params = {
            "guestId": "aut",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": "success",
    "guest": {
        "id": 4,
        "name": "Jessica Jones",
        "email": "janey@doe.com",
        "comment": "Hey I love your work and I want to see you"
    }
}
```
> Example response (404):

```json
{
    "status": "error",
    "message": "guest not found!"
}
```
> Example response (500):

```json
null
```

### HTTP Request
`GET /guests/get/{guestId}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    guestId |  required  | The id of the guest.

<!-- END_52a8a371e7a1dc53ea827dae3ae65cca -->

<!-- START_2ae99d85ce711b8991fa2d8f86127c78 -->
## /guests/delete/{guestId}
> Example request:

```bash
curl -X DELETE "/guests/delete/1?guestId=quo" 
```

```javascript
const url = new URL("/guests/delete/1");

    let params = {
            "guestId": "quo",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": "success",
    "message": "User deleted"
}
```
> Example response (404):

```json
{
    "status": "error",
    "message": "guest not found!"
}
```
> Example response (500):

```json
{
    "status": "error",
    "message": "Unable to delete guest! Encountered an error"
}
```

### HTTP Request
`DELETE /guests/delete/{guestId}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    guestId |  required  | The id of the guest.

<!-- END_2ae99d85ce711b8991fa2d8f86127c78 -->

<!-- START_7c3bd4507b5ed4255fa671e0a13f9fbe -->
## /guests/create
> Example request:

```bash
curl -X POST "/guests/create" \
    -H "Content-Type: application/json" \
    -d '{"name":"Olamide Badoo","email":"jamesbond@kola.com","comment":"Hii I am 10x Engineer"}'

```

```javascript
const url = new URL("/guests/create");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "name": "Olamide Badoo",
    "email": "jamesbond@kola.com",
    "comment": "Hii I am 10x Engineer"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": "success",
    "message": "guest created!"
}
```
> Example response (400):

```json
{
    "status": "error",
    "message": "Invalid  request!",
    "errors": []
}
```
> Example response (500):

```json
{
    "status": "error",
    "message": "Unable to get guests! Encountered an error"
}
```

### HTTP Request
`POST /guests/create`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | The name of the guest.
    email | string |  required  | email of the guest.
    comment | string |  required  | comment of the guest.

<!-- END_7c3bd4507b5ed4255fa671e0a13f9fbe -->

<!-- START_603307de09458091f86de7f6de0b1edc -->
## /guests/update
> Example request:

```bash
curl -X PUT "/guests/update" \
    -H "Content-Type: application/json" \
    -d '{"name":"Olamide Badoo","email":"jamesbond@kola.com","comment":"Hii, I'm a 10x Engineer"}'

```

```javascript
const url = new URL("/guests/update");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "name": "Olamide Badoo",
    "email": "jamesbond@kola.com",
    "comment": "Hii, I'm a 10x Engineer"
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "status": "success",
    "message": "guest created!"
}
```
> Example response (400):

```json
{
    "status": "error",
    "message": "Invalid  request!",
    "errors": []
}
```
> Example response (500):

```json
{
    "status": "error",
    "message": "Unable to get guests! Encountered an error"
}
```

### HTTP Request
`PUT /guests/update`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | The name of the guest.
    email | string |  required  | email of the guest.
    comment | string |  required  | comment of the guest.

<!-- END_603307de09458091f86de7f6de0b1edc -->


