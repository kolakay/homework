<?php

use Illuminate\Database\Seeder;

class GuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $guests = [
            [
                "name" => "Frank DE Nero",
                "email" => "frank@yahoomail.com",
                "comment" => "Testing frank out"
            ],
            [
                "name" => "Chris Martin",
                "email" => "chris@yahoomail.com",
                "comment" => "Testing Chris out"
            ],
            [
                "name" => "Emeli Clark",
                "email" => "emeli@yahoomail.com",
                "comment" => "Testing Emeli out"
            ],
            [
                "name" => "Denzel Washington",
                "email" => "denzel@yahoomail.com",
                "comment" => "Testing Denzel out"
            ],
            [
                "name" => "Robert DE Nero",
                "email" => "robert@yahoomail.com",
                "comment" => "Testing Robert out"
            ]
        ];

        foreach($guests as $guest){
            $newGuest = new App\Models\GuestModel();
            $newGuest->name = $guest["name"];
            $newGuest->email = $guest["email"];
            $newGuest->comment = $guest["comment"];
            $newGuest->save();
        }
    }
}
