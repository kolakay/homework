<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'HomeController@index');

$router->get('/guests', 'HomeController@index');
$router->get('/guests/get/{guestId}', 'HomeController@guest');
$router->delete('/guests/delete', 'HomeController@deleteGuest');
$router->post('/guests/create', 'HomeController@create');
$router->put('/guests/update', 'HomeController@update');
