const Constants = (function () {
  const BASE_URL = 'https://guestbookapi.testmynewwork.website/guests/';

  return {
    API_BASE_URL: BASE_URL,
    API_GET_GUESTS : BASE_URL + '',
    API_CREATE_GUEST: BASE_URL + 'create',
    API_UPDATE_GUEST: BASE_URL + 'update',
    API_DELETE_GUEST: BASE_URL + 'delete',

  }
})();

export default Constants;
