# Visitor's Guestbook Front End

Visitors guests book is a Nuxtjs project for logging guests names and comments

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

you need to have guest-book-api up and running

### Backend Setup

Change API base URL at @common/Constants to the URL that points to guest-book-api


## Build Setup

``` bash
$ cd /path/to/homework/guest_book
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

